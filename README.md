[![Codacy Badge](https://api.codacy.com/project/badge/Grade/176fcdba9feb47e5ba7a33efefd835c8)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jelofson/spot-tools&amp;utm_campaign=Badge_Grade)

Readme
=======
This script and associated libraries are used to generate [SpotORM](http://phpdatamapper.com/) entity files from existing database tables. Primarily, they are used to create the
`fields` array that maps the columns in the table. Creating that array by hand can be a little tedious.

The script creates two files. One file is the Entity class which must extend \Spot\Entity (either by a base Entity which extends \Spot\Entity, or by
extending \Spot\Entity directly. The second file is the fields metadata array. These two files are separated because it makes regenerating
the metadata easy, without disturbing the entity class. 

This script does NOT create relationship definitions, but provides a placeholder method where you can define them after creating the entity. 

Installation
--------
This is a PHP script, so you have to have PHP installed as a CLI for this to work.

* `composer require vespula/spot-tools`. If you don't know what composer is,
[see composer's documentation for more information](https://getcomposer.org/). 
 

* You will need a config file named `spot-config.php`. There is a sample one in the source folder. Your config file should look like this:

```
<?php
return [
    'db_url'=>'mysql://user:passwd@localhost/dbname',
    'entity_folder'=>'/path/to/entities'
];
```

* Your config file will be loaded automatically if you have it in the same folder as where you are running the script from. 
For example, at the same location as your composer.json file. 

* You can specify a config file using the `-f` switch.

```
$ ./vendor/bin/spot-entity -t mytable -f src/Vendor/spot-config.php
```


Usage
----

* To get help with the command line options, type `./vendor/bin/spot-entity -h`

* Try a preview by typing: `./vendor/bin/spot-entity -t mytable -p`

* This will output the fields metadata and the entity class to the screen without writing any files. This might be handy to see what would be generated. 

* To generate the files with default options use:  
`./vendor/bin/spot-entity -t mytable`

That will create two files in the configured output folder. The first is the entity class and is named `Mytable.php`. The second will be the
metadata file saved in the `metadata` subfolder. It will be called `Mytable.php` as well. 

If you open the entity class that was generated, you will see a method called `fields()` that will reference the metadata file via an include statement.
This is beneficial as you can re-run the `./vendor/bin/spot-tools` script and opt to overwrite the metadata field definitions without replacing or disturbing
the entity file.

Command Options
---------
```
Usage: ./bin/spot-entity [-c classname, --classname classname] [-e extends, --extends extends (default: \Spot\Entity)] [-f file, --file file (default: spot-config.php)] [-h help, --help help] [-n namespace, --namespace namespace] [-o, --overwrite] [-p, --preview] [-t table, --table table]

Required Arguments:
        -t table, --table table
                The table you want to reverse

Optional Arguments:
        -f file, --file file (default: spot-config.php)
                The config file you want to use. Defaults to `spot-config.php`.
        -e extends, --extends extends (default: \Spot\Entity)
                The class this entity should extend
        -n namespace, --namespace namespace
                The namespace for this class
        -p, --preview
                Write out the class to screen to preview
        -c classname, --classname classname
                The name of the entity if you don't want to use the defaults.
        -o, --overwrite
                Overwrite existing metadata field file
        -h help, --help help
                Show help
```