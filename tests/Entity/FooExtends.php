<?php


use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

/**
 * Creates a Spot\Entity object which represents a database table
 * 
 * See http://phpdatamapper.com/
 */

class Foo extends \Bar 
{
    /**
     * @var string The name of the table that this entity represents
     */
    protected static $table = 'foo';

    /**
     * Describe the fields (columns) associated with this entity
     * 
     * @return array Field descriptions
     */
    public static function fields()
    {
        $metadata = include 'metadata/Foo.php';
        return $metadata;
    }
    
    /**
     * Configure relationships between entities
     * 
     * @param Mapper $mapper A mapper interface
     * @param Entity $entity The current entity instance
     * @return array Relationship definitions
     */
    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [

        ];
    }
}
