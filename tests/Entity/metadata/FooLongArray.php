<?php
return array (
    'id' => array (
        'type' => 'integer',
        'autoincrement' => true,
        'primary' => true,
    ),
    'name' => array (
        'type' => 'string',
        'length' => 25,
        'required' => true,
    ),
);
