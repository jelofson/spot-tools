<?php
return [
    'id' => [
        'type' => 'integer',
        'autoincrement' => true,
        'primary' => true,
    ],
    'name' => [
        'type' => 'string',
        'length' => 25,
        'required' => true,
    ],
];
