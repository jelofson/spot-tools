<?php
namespace Vespula\SpotTools;
use Vespula\SpotTools\Reflector;
use PDO;

class ReflectorTest extends \PHPUnit_Framework_TestCase
{
    
    public function setUp()
    {
        $pdo = new PDO('sqlite:' . sys_get_temp_dir() . '/reflectorTest.sq3'); 
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->createDatabase($pdo);

    }
    
    public function tearDown()
    {
        unlink(sys_get_temp_dir() . '/reflectorTest.sq3');
    }
    
    protected function createDatabase($pdo)
    {
        $sql_log = 'CREATE TABLE log ('
                . 'id INTEGER PRIMARY KEY, '
                . 'level VARCHAR(50), '
                . 'uid VARCHAR(15), '
                . 'message TEXT, '
                . 'timestamp TEXT'
                . ')';
        
        $pdo->query($sql_log);
        $pdo->query('CREATE UNIQUE INDEX idx_uid on log(uid)');
        $pdo->query('CREATE INDEX idx_level on log(level)');

    }
    
    public function testGetFields()
    {
        $dbal_config = new \Doctrine\DBAL\Configuration();
        $params = [
            'url'=>'sqlite:///' . sys_get_temp_dir() . '/reflectorTest.sq3'
        ];
        $conn = \Doctrine\DBAL\DriverManager::getConnection($params, $dbal_config);
        $schemaManager = $conn->getSchemaManager();
        
        $reflector = new Reflector($conn, $schemaManager);
        $fields = $reflector->getFields('log');
        
        
        $expected = [
            'id'=>[
                'primary'=>true,
                'autoincrement'=>true,
                'type'=>'integer',
                'length'=>10
            ],
            'level'=>[
                'type'=>'string',
                'length'=>50,
                'index'=>true
            ],
            'uid'=>[
                'type'=>'string',
                'length'=>15,
                'unique'=>true
            ],
            'message'=>[
                'type'=>'text'
            ],
            'timestamp'=>[
                'type'=>'text'
            ]
        ];
        $this->assertEquals($expected, $fields);
    }
}
