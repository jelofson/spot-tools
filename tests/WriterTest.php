<?php

namespace Vespula\SpotTools;

use Vespula\SpotTools\Writer;

class WriterTest extends \PHPUnit_Framework_TestCase
{
    
    protected $table = 'foo';
    protected $classname = 'Foo';
    protected $fields = [
        'id'=>[
            'type'=>'integer',
            'autoincrement'=>true,
            'primary'=>true
        ],
        'name'=>[
            'type'=>'string',
            'length'=>25,
            'required'=>true
        ]
    ];
    
    protected function setUp()
    {
        if (file_exists(sys_get_temp_dir() . '/Foo.php')) {
            unlink(sys_get_temp_dir() . '/Foo.php');
        }
        
        if (file_exists(sys_get_temp_dir() . '/Bar.php')) {
            unlink(sys_get_temp_dir() . '/Bar.php');
        }
        
        if (file_exists(sys_get_temp_dir() . '/metadata/Foo.php')) {
            unlink(sys_get_temp_dir() . '/metadata/Foo.php');
        }
    }
    
    
    public function testWriteToFile()
    {

        $writer = new Writer($this->table, $this->fields, $this->classname);
        
        $writer->writeToFile(sys_get_temp_dir());
        
        $outputFile = $writer->getOutputFile();
        $expectedFile = sys_get_temp_dir() . '/Foo.php';
        $this->assertEquals($expectedFile, $outputFile);
        
        $expectedString = file_get_contents(__DIR__ . '/Entity/Foo.php');
        $actualString = file_get_contents($outputFile);
        
        $this->assertEquals($expectedString, $actualString);
        
    }
    
    public function testWriteFields()
    {
        $writer = new Writer($this->table, $this->fields, $this->classname);
        
        $writer->writeFields(sys_get_temp_dir());
        
        $expected = file_get_contents(__DIR__ . '/Entity/metadata/Foo.php');
        $actual = file_get_contents(sys_get_temp_dir() . '/metadata/Foo.php');
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testSetShortArray()
    {
        
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->setShortArray(false);
        $writer->writeFields(sys_get_temp_dir());
        
        $expected = file_get_contents(__DIR__ . '/Entity/metadata/FooLongArray.php');
        $actual = file_get_contents(sys_get_temp_dir() . '/metadata/Foo.php');
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testSetExtends()
    {
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->setExtends('\Bar');
        $writer->writeToFile(sys_get_temp_dir());
        
        $expected = file_get_contents(__DIR__ . '/Entity/FooExtends.php');
        $actual = file_get_contents(sys_get_temp_dir() . '/Foo.php');
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testSetNamespace()
    {
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->setNamespace('Bar');
        $writer->writeToFile(sys_get_temp_dir());
        
        $expected = file_get_contents(__DIR__ . '/Entity/FooNS.php');
        $actual = file_get_contents(sys_get_temp_dir() . '/Foo.php');
        
        $this->assertEquals($expected, $actual);
    }
    
    public function testNoOverwriteFields()
    {
        $this->setExpectedException(
            'Exception',
            'File exists (' . sys_get_temp_dir() . '/metadata/Foo.php) and overwrite flag not set. See bin/generate -h for help'
        );
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->writeFields(sys_get_temp_dir());
        
        // write again
        $writer->writeFields(sys_get_temp_dir());
        
        
    }
    
    public function testNoFolder()
    {
        $this->setExpectedException(
            'Exception',
            'ERROR: Missing output folder for entity.'
        );
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->writeToFile('');

    }
    
    public function testNoMetadataFolder()
    {
        $this->setExpectedException(
            'Exception',
            'ERROR: Missing output folder for entity and metadata.'
        );
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->writeFields('');

    }
    
    public function testFolderNotWritable()
    {
        // This had better throw an exception!!
        $this->setExpectedException(
            'Exception',
            'ERROR: Output folder (/) is not writable.'
        );
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->writeToFile('/');

    }
    
    public function testMetadataFolderNotWritable()
    {
        // This had better throw an exception!!
        $this->setExpectedException(
            'Exception',
            'ERROR: Output folder (/) is not writable.'
        );
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->writeFields('/');

    }
    
    public function testSetTemplate()
    {
        
        $writer = new Writer($this->table, $this->fields, $this->classname);
        $writer->setTemplate(__DIR__ . '/template/entity.php');
        $writer->writeToFile(sys_get_temp_dir());
        
        $expected = file_get_contents(__DIR__ . '/template/entity.php');
        $actual = file_get_contents(sys_get_temp_dir() . '/Foo.php');
        
        $this->assertEquals($expected, $actual);

    }
    
    public function testClassname()
    {
        $writer = new Writer($this->table, $this->fields, 'Bar');
        
        $writer->writeToFile(sys_get_temp_dir());
        
        
        $expected = file_get_contents(__DIR__ . '/Entity/Bar.php');
        $actual = file_get_contents(sys_get_temp_dir() . '/Bar.php');
        $this->assertEquals($expected, $actual);
    }
    
}
