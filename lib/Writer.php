<?php

namespace Vespula\SpotTools;

/**
 * Class to output the generated entity class and field metadata files.
 * 
 * Use the \SpotTools\Reflector to get the fields data from a given table
 * 
 */
class Writer
{
    /**
     *
     * @var string The table name to generate an entity for
     */
    protected $table;
    /**
     *
     * @var array The field metadata acquired by using \SpotTools\Reflector
     */
    protected $fields = [];
    /**
     *
     * @var string The entity class name. If not passed to constructor, DBAL inflector will 
     * be used to create the singlular classname
     */
    protected $className;
    /**
     *
     * @var string The template to create the entity object from
     */
    protected $template;
    /**
     *
     * @var string The full path to the output file. For information purposes.
     */
    protected $outputFile;
    /**
     *
     * @var bool Whether to use short array syntax or not.
     */
    protected $shortArray = true;
    
    /**
     *
     * @var string The Entity namespace
     */
    protected $namespace;
    /**
     *
     * @var string The class to extend
     */
    protected $extends = '\Spot\Entity';
    /**
     *
     * @var bool Overwrite existing metadata files or not.
     */
    protected $overwrite = false;
    
    
    /**
     * Constructor
     * 
     * @param string $table The table name
     * @param array $fields The fields
     * @param string $classname Class name
     */
    public function __construct($table, $fields, $classname)
    {
        $cwd = dirname(__DIR__);
        $this->template = $cwd . '/template/entity.php';
        $this->table = $table;
        $this->fields = $fields;
        $this->className = $classname;
    }

    /**
     * Set the template file
     * 
     * @param string $file The template file
     * @return $this This class
     */
    public function setTemplate($file)
    {
        $this->template = $file;
        return $this;
    }
    
    /**
     * Set short array flag. True to use short array syntax
     * 
     * @param bool $shortArray
     * @return $this
     */
    public function setShortArray($shortArray)
    {
        $this->shortArray = (bool) $shortArray;
        return $this;
    }
    
    /**
     * Set indent flag. True to indent the fields
     * 
     * This is purely a cosmetic feature for code output.
     * 
     * @param bool $indent
     * @return $this
     */
    public function setIndent($indent)
    {
        $this->indent = (bool) $indent;
        return $this;
    }
    
    /**
     * Set the output file namespace
     * 
     * @param string $namespace
     * @return $this
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
        return $this;
    }
    
    /**
     * The class the entity should extend
     * 
     * @param string $extends
     * @return $this
     */
    public function setExtends($extends)
    {
        $this->extends = $extends;
        return $this;
    }
    /**
     * Set the overwrite flag. Overwrites metadata file
     * 
     * @param bool $overwrite
     */
    public function setOverwrite($overwrite)
    {
        $this->overwrite = (bool) $overwrite;
    }
    
    /**
     * Write the entity class to file, but only if it doesn't exist.
     * 
     * @param string $folder
     * @return mixed Bytes written or false on failure
     * @throws \Exception
     */
    public function writeToFile($folder = '.')
    {
        if (empty($folder)) {
            throw new \Exception('ERROR: Missing output folder for entity.');
        }
        
        if (!is_writable($folder)) {
            throw new \Exception('ERROR: Output folder (' . $folder . ') is not writable.');
        }
        
        $output = $this->buildTemplate();

        $folder = rtrim($folder, '/');

        $filename = $folder . '/' . $this->className . '.php';
        
        $this->outputFile = $filename;
        
        if (file_exists($filename)) {
            throw new \Exception('File exists (' . $this->outputFile . '). This script will not overwrite existing entities.');
        }
        return file_put_contents($filename, $output);
    }
    
    /**
     * Write the output to screen for preview purposes.
     * 
     */
    public function writeToScreen()
    {
        $fieldsString = $this->processFields($this->fields);
        $class = $this->buildTemplate();
        $output = [];
        $output[] = "// Displaying the field definitions...";
        $output[] = $fieldsString;
        $output[] = '---------------------------------';
        $output[] = "// Displaying class";
        $output[] = $class;
        
        echo implode(PHP_EOL, $output);
        
    }
    
    /**
     * Write the fields metadata to file and overwrite if flag set and file exists
     * 
     * @param string $folder
     * @return mixed Bytes written on success
     * @throws \Exception
     */
    public function writeFields($folder = '.')
    {
        if (empty($folder)) {
            throw new \Exception('ERROR: Missing output folder for entity and metadata.');
        }
        // Ensure the metadata folder exists and is writable. Try to create the 
        // metadata folder if it doesn't exist.
        
        
        if (!is_writable($folder)) {
            throw new \Exception('ERROR: Output folder (' . $folder . ') is not writable.');
        }
        
        $folder = rtrim($folder, '/');
        
        // Ensure a metadata folder. If not, create it.
        
        if (!file_exists($folder . '/metadata')) {
            if (!mkdir($folder . '/metadata')) {
                throw new \Exception('ERROR: Metadata folder does not exist and could not be created.');
            }
        }
        
        $fieldsString = $this->processFields($this->fields);
        
        
        $filename = $folder . '/metadata/' . $this->className . '.php';
        
        $this->outputFile = $filename;
        
        if (file_exists($filename) && ! $this->overwrite) {
            throw new \Exception('File exists (' . $this->outputFile . ') and overwrite flag not set. See bin/generate -h for help');
        }
        
        $output = '<?php' . PHP_EOL;
        $output .= 'return ' .$fieldsString . ';' . PHP_EOL;
        $success = file_put_contents($filename, $output);
        if ($success === false) {
            throw new \Exception('ERROR: Failed to write metadata to output folder.');
        }
        
        // Return the number of bytes written, for good measure.
        return $success;
    }
    
    /**
     * Get the full path to the output file.
     * 
     * @return string
     */
    public function getOutputFile()
    {
        return $this->outputFile;
    }
    
    /**
     * Build the class from a template file.
     * 
     * @return string The output template as string
     */
    protected function buildTemplate()
    {
        $template = file_get_contents($this->template);
        
        if ($template === false) {
            throw new \Exception('ERROR: Could not load template (' . $this->template . ')');
        }
        
        $template = str_replace('{extends}', $this->extends, $template);
        $namespace = '';
        if ($this->namespace) {
            $namespace = 'namespace ' . $this->namespace . ';' . PHP_EOL;
        }
        $template = str_replace('{namespace}', $namespace, $template);
        $template = str_replace('{classname}', $this->className, $template);
        $template = str_replace('{table}', $this->table, $template);
        
        $metadataFile = 'metadata/'. $this->className . '.php';
        
        $template = str_replace('{metadata}', $metadataFile, $template);
        return $template;
    }

    /**
     * Process the array of fields into php code.
     * 
     * @param array $fields
     * @param bool $indent Indent the output or not
     * @return string The fields string for insertion in the metadata file
     */
    protected function processFields($fields)
    {
        $fieldsString = var_export($fields, true);

        $fieldsString = str_replace('0 => ' , '', $fieldsString);
        $fieldsString = str_replace('1 => ' , '', $fieldsString);
        $fieldsString = str_replace("=> \n" , '=> ', $fieldsString);
        $fieldsString = str_replace("    array (" , 'array (', $fieldsString);
        $fieldsString = str_replace("  array (" , 'array (', $fieldsString);

        if ($this->shortArray) {
            $fieldsString = str_replace('array (' , '[', $fieldsString);
            $fieldsString = str_replace(')' , ']', $fieldsString);
        }
        
        $fieldsString = str_replace('  ' , '    ', $fieldsString);
        $fieldsString = str_replace("\t" , '    ', $fieldsString);
        
        
        return $fieldsString;
    }
}
