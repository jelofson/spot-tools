<?php
namespace Vespula\SpotTools;

use \Doctrine\DBAL\Connection;
use \Doctrine\DBAL\Schema\AbstractSchemaManager as SchemaManager;

/**
 * This class examines a db table using Doctrine's DBAL and generates an array 
 * of fields based on Doctrine's column types. This array of fields is then used by 
 * the \SpotTools\Writer to generate and entity class and metadata file.
 * 
 */
class Reflector 
{    
    /**
     * the Doctrine connection
     * 
     * @var \Doctrine\DBAL\Connection 
     */
    private $conn;
    /**
     * The schema manager
     * 
     * @var \Doctrine\DBAL\Schema\AbstractSchemaManager 
     */
    private $schemaManager;
    
    /**
     * Constructor
     * 
     * @param Connection $conn
     * @param SchemaManager $schemaManager
     */
    public function __construct(Connection $conn, SchemaManager $schemaManager)
    {
        $this->conn = $conn;
        $this->schemaManager = $schemaManager;
    }
    
    /**
     * Get fields from the database
     * 
     * @param string $table
     * @return mixed False on failure, array on success
     */
    public function getFields($table)
    {
        $tableDetails = $this->schemaManager->listTableDetails($table);
        $columns = $tableDetails->getColumns();
        if (! $columns) {
            return false;
        }
        
        $primary = [];
        if ($tableDetails->hasPrimaryKey()) {
            $primary = $tableDetails->getPrimaryKeyColumns();
        }
        
        $indexes = $tableDetails->getIndexes();
        $foreignKeys = $tableDetails->getForeignKeys();
        
        $fields = [];
        foreach ($columns as $column) {
            $fields[$column->getName()] = $this->buildFieldOptions($column, $primary, $indexes, $foreignKeys);
        }
        
        return $fields;
    }
    
    /**
     * Build the field options array.
     * 
     * @param  \Doctrine\DBAL\Schema\Column $column
     * @param array $primary Array of primary columns
     * @param array $indexes Array of doctrine indexes
     * @param array $foreignKeys Array of foreign keys
     * 
     * @return array
     */
    protected function buildFieldOptions($column, $primary, $indexes, $foreignKeys) 
    {
        $name = $column->getName();
        $options = [
            'type'=>$column->getType()->getName(),
            'primary'=>false,
            'index'=>false,
            'unique'=>false,
        ];
        if (in_array($name, $primary)) {
            $options['primary'] = true;
        }
        if ($column->getAutoincrement()) {
            $options['autoincrement'] = true;
        }
        if ($column->getNotnull() && !$column->getAutoincrement()) {
            $options['required'] = true;
        }
        if ($column->getLength()) {
            $options['length'] = $column->getLength();
        }

        if ($column->getType()->getName() == 'integer') {
            $options['length'] = $column->getPrecision();
        }

        if ($column->getType()->getName() == 'decimal') {
            $options['length'] = [$column->getPrecision(), $column->getScale()];
        }

        if ($column->getDefault()) {
            $options['default'] = $column->getDefault();
        }
        
        $options = $this->getOptionIndexes($name, $options, $indexes);
        $options = $this->getOptionFKeys($name, $options, $foreignKeys);
        $options = $this->simplifyFieldOptions($options);


        return $options;

    }
    
    /**
     * Check the table indexes and determine if the column has an index or not
     * 
     * Indexes will be of type `index` or `unique`
     * 
     * @param string $name The column name
     * @param array $options Column options
     * @param array $indexes Table indexes
     * @return array The column options
     */
    protected function getOptionIndexes($name, $options, $indexes)
    {
        foreach ($indexes as $index) {
            $indexCols = $index->getColumns();
            if (in_array($name, $indexCols)) {
                $options['index'] = $index->isSimpleIndex();
                $options['unique'] = $index->isUnique();
            }
            
        }
        
        return $options;
    }
    
    /**
     * Compare the table foreign keys against the column and set a foreign key 
     * as necessary 
     * 
     * @param string $name The column name
     * @param array $options The column options
     * @param array $foreignKeys Foreign keys on the table
     * @return array The column options
     */
    protected function getOptionFKeys($name, $options, $foreignKeys)
    {
        foreach ($foreignKeys as $fk) {
            $fkCols = $fk->getLocalColumns();
            if (in_array($name, $fkCols)) {
                $options['foreignkey'] = true;
            }
        }
        
        return $options;
    }


    /**
     * Clean up the options array by removing redundant and unnecessary elements.
     * 
     * @param array $options
     * @return array 
     */
    protected function simplifyFieldOptions($options)
    {
        // Ignore primary and unique. If primary, then already unique
        // Unset primary = false
        
        if ($options['primary'] && isset($options['unique']) && $options['unique'] === true) {
            $options['unique'] = false;
        }
        if ($options['primary'] === false) {
            unset($options['primary']);
        }
        
        if ($options['index'] === false) {
            unset($options['index']);
        }
        if ($options['unique'] === false) {
            unset($options['unique']);
        }
        return $options;
    }
    
    
}

